const path = require('path');
const { User } = require(path.join(__dirname,'..','..','models','index'));
const redis = require(path.join(__dirname,'..','..','redis'));
const { getValues } = require('sequelize-values')();

const confirmEmailController = async (req, res) => {
    const { id } = req.params;
    const userId = await redis.get(id);
    var result;
    
    if(userId){
        result = await User.update({
            confirmed: true 
        },{
            where: {id:userId}
        });
    } else {
        return res.send("F.O M.O.F.O");
    }
    
    const val = getValues(result);
    
    if(val) {
        await redis.del(id);
        return res.send("LULzzzzz");
    } else {
        return res.send("failure");
    }
    
};

module.exports = {
    confirmEmailController
};