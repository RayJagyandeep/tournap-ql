const path = require('path');

const { GraphQLServer } = require('graphql-yoga');
const db = require(path.join(__dirname,'..','models','index'));
const controller = require(path.join(__dirname,'controllers','index'));
const redis = require(path.join(__dirname,'..','redis'));
const { generateSchema } = require(path.join(__dirname,'utils','utils'));

const startServer = async () => {
    const mergedSchemas = await generateSchema();
    await db.sequelize.authenticate;
    const server = new GraphQLServer({ 
        schema: mergedSchemas,
        context: (req) =>{
            return ({
                db,
                redis,
                url: req.request.protocol + "://" + req.request.get('host')
            });
        } 
    });
    server.express.get('/confirm/:id', await controller.confirmEmailController);
    console.log("Database Connected successfully");

    const app = await server.start({
        port: process.env.NODE_ENV === 'test' ? 0 : 4000
    });
    
    console.log('Server is running on localhost:'+app.address().port);
    return app;
};

module.exports = startServer;