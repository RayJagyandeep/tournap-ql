const { getValues } = require('sequelize-values')();
const bcrypt = require('bcrypt');

const resolvers = {
    Mutation: {
        login: async(_, args, { db }) => {
            const email = args.email;
            const password = args.password;
            const userModel = await db.User.findOne({
                where: { email },
                select: ['id', 'password','confirmed']
            });
            if(!userModel){
                return [{
                    path:'email',
                    message:'Invalid credentials'
                }];
            }
            const userData = getValues(userModel);
            if(!userData.confirmed){
                return [{
                    path:'email',
                    message:'Email not confirmed'
                }];
            }
            if (!bcrypt.compareSync(password,userData.password)){
                return [{
                    path:'email',
                    message: 'invalid credentials'
                }];
            }
            return null;
        }
    }
};
module.exports = resolvers;