const path = require('path');
const bcrypt = require('bcrypt');
var yup = require('yup');
const { getValues } = require('sequelize-values')();

const { User, HashTag } = require(path.join(__dirname,'..','..','..','models','index'));
const conf = require(path.join(__dirname,'..','..','..','config','appconfig.json'));
const errorFormat = require(path.join(__dirname,'..','..','utils','errorFormat'));
const { duplicateRegistration } = require(path.join(__dirname,'..','..','utils','errorMessages'));
const { generateConfirmationEmaiLink, sendConfirmationEmail }  = require(path.join(__dirname,'..','..','utils','utils'));

const schema = yup.object().shape({
  firstName: yup.string().min(3).max(255),
  lastName: yup.string().min(3).max(255),
  email: yup.string().min(3).max(255).email(),
  password: yup.string().min(3).max(255)
});

const resolvers = {
    Query: {
      hello: async (_, args, {db}) => {
        var HashTagModel;
        HashTagModel = await db.HashTag.findAll({
          attributes:["hash_tag_id","hash_tag_name"]
        });
        const data = getValues(HashTagModel);
        return `Bye ${args.name || "world"}`;
      }
    },

    Mutation: {
      register: async (_, args, { db ,redis, url }) => {
        var userModel;

        try {
          await schema.validate(args);
        } catch (err) {
          return errorFormat(err);
        }

        const { firstName, lastName, email, password } = args;

        userModel = await db.User.findOne({
          where: {email},
          select: ["id"]
        });

        if(userModel) {
         return duplicateRegistration;
        }

        var passwordEnc = bcrypt.hashSync(password,conf.dev.SaltRound);
        userModel = await db.User.create({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: passwordEnc,
          confirmed:false
      });
        const data = getValues(userModel);
       
        await sendConfirmationEmail(email,await generateConfirmationEmaiLink(url,data.id, redis));

        return null;

        /**** Using Build *****/
        /*
        var task = await db.User.build({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: passwordEnc
        });
        var usr = await task.save();
        console.log(usr);
        return true
        */
      }
    }
  };

  module.exports = resolvers;