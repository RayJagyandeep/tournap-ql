const path = require('path');
const { getValues } = require('sequelize-values')();
const redis = require(path.join(__dirname,'..','..','..','..','redis'));
var fetch = require('node-fetch');
const { User } = require(path.join(__dirname,'..','..','..','..','models','index'));
const { generateConfirmationEmaiLink } = require(path.join(__dirname,'..','..','..','utils','utils'));


let id, userModel, userData;

describe('Test - ConfirmEmail link', async() => {

    beforeAll(async() => {
        //Create a User
        userModel = await User.create({
            firstName: "testJohn",
            lastName: "testDoe",
            email: "testjohndoe@test.com",
            password: "passwordtest",
            confirmed:false
        });
        userData = getValues(userModel);
        id = userData.id;
    });

    it('should test a valid confirm email link and check redis key is removed', async() => {
        const url = await generateConfirmationEmaiLink(
            process.env.TEST_HOST,
            id,
            redis
        );
        
        var resp = await fetch(url);
        const text = await resp.text();
        expect(text).toEqual("LULzzzzz");
        userModel = await User.findOne({
            where: {id},
            select:['confirmed']
        }); 
        userData = getValues(userModel);
        const chunks = url.split('/');
        const key = chunks[chunks.length-1];
        const redisKey = await redis.get(key);
        expect(userData.confirmed).toBeTruthy();
        expect(redisKey).toBeNull();

    });

    it('should test an invalid confirm email link', async() => {
        const resp = await fetch(`${process.env.TEST_HOST}/confirm/12345`);
        const text = await resp.text();
        expect(text).toEqual('F.O M.O.F.O');
    });

    afterAll(async() => {
        await User.truncate();
    });
});