const path = require('path');
const startServer = require(path.join(__dirname,'..','..','startServer'));

const globalSetup = async() => {
    const app = await startServer();
    const { port } = app.address();
    process.env.TEST_HOST = `http://localhost:${port}`;
   // await sequelize.authenticate();
  };

  module.exports = globalSetup;
  