const path = require('path');
const _ = require('lodash');

const  { 
  User, 
  sequelize 
}  = require(path.join(__dirname,'..','..','..','..','models','index'));

const { 
  duplicateRegistration, 
  insufficentPassword, 
  incorrectEmail 
} = require(path.join(__dirname,'..','..','..','utils','errorMessages'));

const { request } = require('graphql-request');


const firstName = 'John';
const lastName = 'Doe';
const email = "john.doe@test.com";
const password = 'johnDoe123#';

const mutation = (f,l,e,p) => `
mutation {
  register(
    firstName:"${f}", 
    lastName:"${l}", 
    email:"${e}", 
    password:"${p}"
    ){
      path
      message
    }
}
`;

describe('Registration test', async() => {
    it('Test valid Registration', async () => {
      const resp = await request(process.env.TEST_HOST,mutation(firstName,lastName,email,password));
      expect(resp).toEqual({register:null});
      const usr = await User.findAll({
        where: {
          email
        }
      });
      expect(usr).toHaveLength(1);
      const user = usr[0];
      expect(user.firstName).toEqual(firstName);
      expect(user.lastName).toEqual(lastName);
      expect(user.email).toEqual(email);
      expect(user.password).not.toEqual(password);
    });
    
    it('Test Duplicate Registration', async () => {
      const resp2 = await request(process.env.TEST_HOST, mutation(firstName,lastName,email,password));
      expect(resp2).toEqual({
        register:duplicateRegistration
      });
    });

    it('Test Invalid Password', async () => {
      const resp3 = await request(process.env.TEST_HOST, mutation(firstName,lastName,email,"jo"));
      expect(resp3).toEqual({
        register:insufficentPassword
      });
    });

    it('Test invalid email', async () => {
      const resp4 = await request(process.env.TEST_HOST, mutation(firstName,lastName,"abc",password));
      expect(resp4).toEqual({
        register:incorrectEmail
      });
    });
});

afterAll(async() => {
  await User.truncate();
  //sequelize.close();
});
