const path = require('path');
const { User } = require(path.join(__dirname,'..','..','models','index'));
const { getValues } = require('sequelize-values')();
const { request } = require('graphql-request');

const testData = {
    firstName:"testlogin",
    lastName:"testlogin",
    email:"testlogin@test.com",
    password:"testlogin@123#"
};
const registerMutation  = `
mutation {
  register(
    firstName:"${testData.firstName}", 
    lastName:"${testData.lastName}", 
    email:"${testData.email}", 
    password:"${testData.password}"
    ){
      path
      message
    }
}
`;

const loginMutation = `
mutation {
  login( 
    email:"${testData.email}", 
    password:"${testData.password}"
    ){
      path
      message
    }
}
`;

let userModel, userData;
beforeAll(async()=>{
    userModel = await User.create({
        firstName: testData.firstName,
        lastName: testData.lastName,
        email: testData.email,
        password: testData.password,
    });
    userData = getValues(userModel);
});

describe('Test Authentication/Login',async()=>{
    it('should try login unconfirmed user', async() => {
        
    });
    
    it('should confirm the user email and then login', () => {
        
    });

    it('should attempt to login invalid user', () => {
        
    });

    it('should attempt to login valid user with invalid password', () => {
        
    });
});

afterAll(async()=>{

});
