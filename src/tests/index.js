const path = require('path');
const globalSetup = require(path.join(__dirname,'config','globalSetup'));

module.exports = async function(){
    await globalSetup();
    return null;
}