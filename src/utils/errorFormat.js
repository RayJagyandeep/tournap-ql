const errorFormat = (err) => {
    const errors = [];
    errors.push(
        {
            path:err.path,
            message:err.message
        }
    )
    
    return errors;
};

module.exports = errorFormat;