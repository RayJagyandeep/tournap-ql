const v4 = require('uuid/v1');
const { importSchema } = require('graphql-import');
const { mergeSchemas, makeExecutableSchema } = require('graphql-tools');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const SparkPost = require('sparkpost');

const utils = {
    generateConfirmationEmaiLink : async (url, userId, redis) => {
         const uuid = v4({
             msecs: new Date().getTime()
         });
        
         await redis.set(uuid, userId, 'EX', 60 * 60 * 24);
         return `${url}/confirm/${uuid}`;
     },

     generateSchema: async() => {
        const schemas = [];
        const folders = fs.readdirSync(path.join(__dirname,'..','modules'));

        _.forEach(folders,(folder) => {
            const  resolvers  = require(path.join(__dirname,'..',`modules`,`${folder}`,'resolver'));
           
            const typeDefs = importSchema(
                path.join(__dirname,`..`,`modules`,`${folder}`,`schema.graphql`)
            );
            schemas.push(
                makeExecutableSchema({
                    typeDefs,
                    resolvers
                })
            );
        });
        return  mergeSchemas({ schemas });
     },

     sendConfirmationEmail: async(reciever, url) => {
        const client = new SparkPost();
        client.transmissions.send({
            options: {
              sandbox: true
            },
            content: {
              from: 'testing@sparkpostbox.com',
              subject: 'Email Confirmation',
              html:`<html>
                        <body>
                            <p>
                                Hi !
                                Welcome to TourNap.! Please on the confirm email link to begin
                                <a href=${url}>Confirm Email</a>
                            </p>
                        </body>
                    </html>`
            },
            recipients: [
              {address: reciever }
            ]
          }).then((data) => {
              console.log(data);
          }).catch(err =>{
              console.error(err);
          });
         // console.log(response);
     }
};


module.exports = utils;