
const Msg = {
    duplicateRegistration:[{
            message:'Already Taken',
            path:'email'
        }],
    insufficentPassword:[{
            message:'password must be at least 3 characters',
            path:'password',
        }],
    tooMuchPassword:[{
            message:'password must be at most 255 characters',
            path:'password'
        }],
    incorrectEmail:[{
            message:'email must be a valid email',
            path:'email'
        }]
}

module.exports = Msg;