'use strict';
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define('User', {
    firstName:{
      type : Sequelize.STRING
    },
    lastName: {
      type:Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    password:{
      type: Sequelize.STRING
    },
    confirmed:{
      type:Sequelize.BOOLEAN
    }
  }, {schema:'discovery'});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};